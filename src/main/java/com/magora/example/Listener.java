package com.magora.example;

import com.amazonaws.services.cloudwatch.model.ResourceNotFoundException;
import com.amazonaws.services.s3.event.S3EventNotification;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class Listener {
    @SqsListener("test-queue")
    private void receiveMessage(String json) {
        Optional.of(json)
                .map(S3EventNotification::parseJson)
                .ifPresent(this::processEvent);
    }

    private void processEvent(S3EventNotification notification) {
        List<S3EventNotification.S3EventNotificationRecord> records = notification.getRecords();

        if (records != null && !records.isEmpty()) {
            try {
                for (S3EventNotification.S3EventNotificationRecord record : records) {
                    S3EventNotification.S3Entity entity = record.getS3();
                    S3EventNotification.S3ObjectEntity object = entity.getObject();

                    String[] values = object.getKey().split("/");
                    if (values.length == 2) {
                        String prefix = values[0].toUpperCase();
                        String name = values[1];

                        System.out.println(prefix + "/" + name);
                    }
                }
            } catch (ResourceNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
