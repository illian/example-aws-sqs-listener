package com.magora.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Alexey Podoinikov
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@Controller
public class AwsController {
    public static final String PREFIX = "original";
    private final SignatureGenerator signatureGenerator;
    private final String bucketName;

    @Autowired
    public AwsController(SignatureGenerator signatureGenerator, @Value("${cloud.aws.s3.bucketname}") String bucketName) {
        this.signatureGenerator = signatureGenerator;
        this.bucketName = bucketName;
    }

    @GetMapping("/")
    public String test(Model model, @RequestParam MultiValueMap<String, String> parameters) {
        if (parameters.containsKey("contenttype")) {
            URL url = signatureGenerator.presignedUrl(bucketName + "/" + PREFIX, UUID.randomUUID().toString(),
                    parameters.get("contenttype").get(0), null);
            model.addAttribute("uploadurl", url.toString());
        }
        return "index";
    }

    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) throws Exception {
        URL url = signatureGenerator.presignedUrl(bucketName + "/" + PREFIX, UUID.randomUUID().toString(),
                file.getContentType(), null);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("PUT");
        connection.setRequestProperty("Content-Type", file.getContentType());
        connection.setDoOutput(true);

        OutputStream osw = connection.getOutputStream();
        osw.write(file.getBytes());
        osw.flush();
        osw.close();

        InputStream inputStream = connection.getInputStream();
        String inputStreamStr = new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining("\n"));
        if (inputStreamStr.isEmpty()) {
            inputStreamStr = "File successfully uploaded";
        }

        redirectAttributes.addFlashAttribute("uploadresult", inputStreamStr);

        return "redirect:/";
    }
}
