package com.magora.example;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.Headers;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.Date;
import java.util.Map;

/**
 * @author shushkov;
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
@Component
public class SignatureGenerator {
    private final AmazonS3 amazonS3;

    @Autowired
    public SignatureGenerator(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    public URL presignedUrl(String bucketName, String savePath, String contentType, Map<String, String> parameters) {
        GeneratePresignedUrlRequest presignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, savePath, HttpMethod.PUT);
        presignedUrlRequest.setMethod(HttpMethod.PUT);
        presignedUrlRequest.setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 20));
        presignedUrlRequest.setContentType(contentType);
        presignedUrlRequest.addRequestParameter(
                Headers.S3_CANNED_ACL,
                CannedAccessControlList.PublicRead.toString()
        );

        if (parameters != null) {
            parameters.entrySet().forEach(
                    entry -> presignedUrlRequest.addRequestParameter(Headers.S3_USER_METADATA_PREFIX + entry.getKey(), entry.getValue())
            );
        }

        return amazonS3.generatePresignedUrl(presignedUrlRequest);
    }
}